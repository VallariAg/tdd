# TDD with pytest in python

## Installing
1. Make virtual Environment
```
pip install virtualenv
virtualenv testing
source testing/bin/activate
```

2. Install pytest
```
pip install pytest
```
3. Run tests
 ```
 pytest
 ```
