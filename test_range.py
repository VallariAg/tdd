from range import Range
import pytest


@pytest.mark.parametrize("range_object, start, end", [
    (Range(4), 0, 4),
    (Range(1, 4), 1, 4),
    (Range(4, 1), 1, 4),
    (Range(-4, 0), -4, 0)
])
def test_init(range_object, start, end):
    assert range_object.start == start
    assert range_object.end == end


@pytest.mark.parametrize("range_object, expected_length", [
    (Range(4), 4),
    (Range(1, 4), 3),
    (Range(4, 1), 3),
    (Range(-4, 0), 4)
])
def test_length(range_object, expected_length):
    assert expected_length == range_object.length()

@pytest.mark.parametrize("range_object, expected_list", [
    (Range(4), list(range(4))),
    (Range(1, 4), list(range(1, 4))),
    (Range(4, 1), list(range(1, 4))),
    (Range(-4, 0), list(range(-4, 0)))
])
def test_to_list(range_object, expected_list):
    assert expected_list == range_object.to_list()

@pytest.mark.parametrize("range_object, n, expected_list", [
    (Range(4), 4, list(range(0, 8))),
    (Range(4), None, list(range(0, 5))),
    (Range(4), -2, list(range(0, 0))),
    (Range(-4, 0), None, list(range(-4, 1))),
])
def test_rstretch(range_object, n, expected_list):
    if n is None:
        range_object.rstretch()
    else:
        range_object.rstretch(n)
    assert expected_list == range_object.to_list()

@pytest.mark.parametrize("range_object, n, expected_list", [
    (Range(4), 4, list(range(-4, 4))),
    (Range(4), None, list(range(-1, 4))),
    (Range(4), -2, list(range(0, 0))),
    (Range(-4, 0), None, list(range(-5, 0))),
])
def test_lstretch(range_object, n, expected_list):
    if n is None:
        range_object.lstretch()
    else:
        range_object.lstretch(n)
    assert expected_list == range_object.to_list()

@pytest.mark.parametrize("range_object, n, expected_list", [
    (Range(4), 4, list(range(-4, 8))),
    (Range(4), None, list(range(-1, 5))),
    (Range(4), -2, list(range(0, 0))),
    (Range(-4, 0), None, list(range(-5, 1))),
])
def test_stretch(range_object, n, expected_list):
    if n is None:
        range_object.stretch()
    else:
        range_object.stretch(n)
    assert expected_list == range_object.to_list()

@pytest.mark.parametrize("range_object, n, expected_list", [
    (Range(4), 4, list(range(4, 8))),
    (Range(4), None, list(range(0, 4))),
    (Range(4, 8), 2, list(range(6, 10))),
    (Range(-4, 0), None, list(range(-4, 0))),
])
def test_shift_right(range_object, n, expected_list):
    if n is None:
        range_object.shift_right()
    else:
        range_object.shift_right(n)
    assert expected_list == range_object.to_list()

@pytest.mark.parametrize("range_object, n, expected_list", [
    (Range(5), 4, list(range(-4, 1))),
    (Range(4, 6), 2, list(range(2, 4))),
    (Range(2, 5), None, list(range(2, 5))),
    (Range(-1, 4), None, list(range(-1, 4))),
])
def test_shift_left(range_object, n, expected_list):
    if n is None:
        range_object.shift_left()
    else:
        range_object.shift_left(n)
    assert expected_list == range_object.to_list()


@pytest.mark.parametrize("range_object, n, expected_list", [
    (Range(5), 2, list(range(2, 3))),
    (Range(3, 8), 2, list(range(5, 6))),
    (Range(4, 8), None, list(range(5, 7))),
    (Range(2, 3), None, list(range(0))),
])
def test_squeeze(range_object, n, expected_list):
    if n is None:
        range_object.squeeze()
    else:
        range_object.squeeze(n)
    assert expected_list == range_object.to_list()

@pytest.mark.parametrize("first_range, second_range, result", [
    (Range(1, 4), Range(4, 7), True),
    (Range(3), Range(4, 5), False),
    (Range(4, 7), Range(1, 4), True),
    (Range(2, 5), Range(2, 3), False),
    (Range(4), Range(5), False),
])
def test_touching(first_range, second_range, result):
    assert first_range.is_touching(second_range) == result

@pytest.mark.parametrize("container, contained, expected_result", [
    (Range(4), Range(0, 4), True),
    (Range(6), Range(4), True),
    (Range(4),  4, False),
    (Range(4),  0, True),
    (Range(0),  0, False),
    (Range(2, 6), Range(3, 5), True),
    (Range(1, 4), Range(3, 5), False),
    (Range(1, 2), Range(4, 5), False),
    (Range(4, 6), Range(1, 2), False)
])
def test_contains(container, contained, expected_result):
        assert container.contains(contained) == expected_result

@pytest.mark.parametrize("range1, range2, expected_result", [
    (Range(4), Range(4), True),
    (Range(6), Range(0, 6), True),
    (Range(2, 6), Range(2, 5), False),
    (Range(-2, 0), Range(-2, 0), True),
])
def test_equals(range1, range2, expected_result):
        assert range1.equals(range2) == expected_result


@pytest.mark.parametrize("range1, range2, expected_result", [
    (Range(4), Range(6, 7), True),
    (Range(4), Range(4, 7), True),
    (Range(4, 7), Range(7, 8), True),
    (Range(0), Range(0), True)
])
def test_is_disjoint(range1, range2, expected_result):
    assert(range1.is_disjoint(range2)) == expected_result
