class Range:
    def __init__(self, *args):
        if len(args) == 1:
            self.start = 0
            self.end = args[0]
        elif len(args) == 2:
            self.start = min(args[0], args[1])
            self.end = max(args[0], args[1])
        else:
            self.reset()
    
    def reset(self):
        self.start = self.end = 0

    def length(self):
        return self.end - self.start

    def to_list(self) -> [int]:
        return list(range(self.start, self.end))

    def rstretch(self, n = 1):
        if n < 0:
            self.reset()
        else:
            self.end += n

    def lstretch(self, n = 1):
        if n < 0:
            self.reset()
        else:
            self.start -= n

    def stretch(self, n = 1):
        if n < 0:
            self.reset()
        else:
            self.start -= n
            self.end += n
    
    def shift_right(self, n = 0):
        self.start += n
        self.end += n
    
    def shift_left(self, n = 0):
        self.start -= n
        self.end -= n

    def squeeze(self, n = 1):
        self.start += n
        self.end -= n
        if (self.start > self.end):
            self.reset()

    def is_touching(self, range2):
        return self.end == range2.start if self.start < range2.start else self.start == range2.end

    def contains(self, x) -> bool:
        if isinstance(x, int):
            return self.start <= x and x < self.end
        return self.start <= x.start and x.end <= self.end

    def equals(self, range2):
        return self.start == range2.start and self.end == range2.end
       
    def is_disjoint(self, range2) -> bool:
        if self.start <= range2.start:
            return self.end <= range2.start
        else:
            return range2.end <= self.start
